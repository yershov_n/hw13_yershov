# Подключитесь к API НБУ ( документация тут https://bank.gov.ua/ua/open-data/api-dev ), получите текущий курс валют и
# запишите его в TXT-файл в таком формате:
#  "[дата создания запроса]"
# 1. [название ввалюты 1] to UAH: [значение курса к валюте 1]
# 2. [название ввалюты 2] to UAH: [значение курса к валюте 2]
# 3. [название ввалюты 3] to UAH: [значение курса к валюте 3]
# ...
# n. [название ввалюты n] to UAH: [значение курса к валюте n]
#
# 2. В вашем репозитории создайте отдельную ветку HW15 от ветки master, реализуйте дз в ней и выполните мердж ветки
# HW15 в ветку master.
import requests


def nbu_response(cur=None, date=None):
    '''
    Return response with currency rate from NBU API

    :param cur: specific currency or None for all currency
    :type cur: str
    :param date: specific date in format YYYYMMDD or None for today
    :type date: str
    :return: response with currency rate
    :rtype: requests.models.Response
    '''

    all_cur = ['AUD', 'CAD', 'CNY', 'HRK', 'CZK', 'DKK', 'HKD', 'HUF', 'INR', 'IDR', 'ILS', 'JPY', 'KZT', 'KRW',
    'MXN', 'MDL', 'NZD', 'NOK', 'RUB', 'SAR', 'SGD', 'ZAR', 'SEK', 'CHF', 'EGP', 'GBP', 'USD', 'BYN', 'RON', 'TRY',
    'XDR', 'BGN', 'EUR', 'PLN', 'DZD', 'BDT', 'AMD', 'IRR', 'IQD', 'KGS', 'LBP', 'LYD', 'MYR', 'MAD', 'PKR', 'VND',
    'THB', 'AED', 'TND', 'UZS', 'TMT', 'RSD', 'AZN', 'TJS', 'GEL', 'BRL', 'XAU', 'XAG', 'XPT', 'XPD']

    if cur is not None and cur not in all_cur:
        raise ValueError

    if date is None:
        pass
    elif len(date) != 8 or not isinstance(int(date), int):
        raise ValueError

    if cur is not None and date is not None:
        url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode={cur}&date={date}&json'
    elif cur is not None:
        url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode={cur}&json'
    elif date is not None:
        url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={date}&json'
    else:
        url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?&json'
    return requests.get(url)


def nbu_result(response):
    '''
    Return f-string with currency rate

    :param response: response from NBU API
    :type response: requests.models.Response
    :return: f-string with currency rate
    :rtype: str
    '''

    res = f'{response.json()[0]["exchangedate"]}\n'
    number = 0
    for item in response.json():
        number += 1
        res += f'{number}. {item["cc"]} to UAH: {item["rate"]}\n'
    return res


def save_txt(any_str, txt_name):
    '''
    Save string in txt-file

    :param any_str: any string
    :type any_str: str
    :param txt_name: name of the file to create
    :type txt_name: str
    '''
    with open(f'{txt_name}.txt', 'w') as file:
        file.write(any_str)


today_resp = nbu_response()
today_result = nbu_result(today_resp)
save_txt(today_result, 'currency_today')

usd_resp = nbu_response('USD')
usd_result = nbu_result(usd_resp)
save_txt(usd_result, 'usd_today')

eur_date_resp = nbu_response('EUR', '20201229')
eur_date_result = nbu_result(eur_date_resp)
save_txt(eur_date_result, 'eur_date')
