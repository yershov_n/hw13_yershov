# Напишите декоратор, замеряющий время выполнения функции.
# Декоратор должен вывести на экран время выполнения задекорированной функции
# Зарегистрируйтесь на https://gitlab.com/ или на https://github.com/.
# Создайте публичный репозиторий и выложите на него это ДЗ. В лмс сдавайте ссылку на репозиторий
from datetime import datetime


def function_timer(func):
    '''
    The decorator displays the execution time of the function.

    :param func: any function
    :type func: function
    :return: displays the execution time of the function
    '''

    def wrapper(*args, **kwargs):
        datetime_before_func = datetime.now()
        res = func(*args, **kwargs)
        print(f'The execution time: {datetime.now()-datetime_before_func}')
        return res

    return wrapper


@function_timer
def foo(num):
    x = 0
    for n in range(num):
        x += 1
    return x


print(foo(10000000))
